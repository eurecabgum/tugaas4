//import liraries
import React from 'react';
import { StyleSheet } from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title, Subtitle, Text, View } from 'native-base';

// create a component
const HeaderComponent = (props, style) => {
    return (
        <Header style={{...style}} {...props}>
            {!props.noLeft && (    
                <Left style={{ flex: 0.3}}>
                    {props.leftIcon && (
                        <Button transparent onPress={props.leftAction}>
                            <Icon name={props.leftIcon} />
                        </Button>
                    )}
                </Left>
            )}
            <Body style={[props.noLeft?{alignItems:'flex-start'}:{alignItems:'center'},{flex:1}]}>
                <Title style={{...props.titleStyle}}>{props.title}</Title>
                {props.subtitle && (
                    <Subtitle style={{color:'grey'}}>{props.subtitle}</Subtitle>
                )}
            </Body>
            <Right style={{ flex: 0.3 }}>
                {props.rightIcon && (
                    <Button badge vertical transparent onPress={props.rightAction}>
                        {/* <Badge style={{width:20, height:20}}><Text style={{fontSize:10, color:'black'}}>2</Text></Badge> */}
                        {props.badgeCount != 0 && (
                            <View style={styles.badgeSection}>
                                <Text style={{fontWeight:'bold', fontSize:8, color:'white'}}>{props.badgeCount}</Text>
                            </View>
                        )}
                        <Icon name={props.rightIcon} />
                    </Button>
                )}

            </Right>
        </Header>
    );
};

HeaderComponent.defaultProps = {
    leftAction: () => console.log('Please insert logic in leftAction props'),
    rightAction: () => console.log('Please insert logic in rightAction props'),
    badgeCount: 0,
    title: 'Header Title'
}

//make this component available to the app
export default HeaderComponent;

// define your styles
const styles = StyleSheet.create({
    badgeSection:{
        backgroundColor:'red', 
        borderRadius:50, 
        height:15, 
        width:15, 
        alignItems:'center', 
        justifyContent:'center',
        position:'absolute',
        zIndex:1,
        left:29
    }
});


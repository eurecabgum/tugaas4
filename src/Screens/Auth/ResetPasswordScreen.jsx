import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import Break from '../../Components/Breaks/Break';
import ButtonRounded from '../../Components/Buttons/ButtonRounded';
import InputBlock from '../../Components/Inputs/InputBlock';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { resetPassword } from '../../services/redux/Actions/ResetPasswordAction';
import { connect } from 'react-redux';

const {width, height} = Dimensions.get('screen')

class ResetPasswordScreen extends Component {
    constructor(){
        super()
        this.state = {
            email: '',
        }
    }

    handleChangeText = inputType => value => {
        this.setState({
            [inputType] : value.replace(/\s/g, '')
        })
    }

    render() {
        const {loadingState:{isLoading}, ResetPassword} = this.props
        return (
            <KeyboardAvoidingView  style={styles.container}>
                <View style={styles.formContainer}>
                    {/* ------------------FormInput----------------------------- */}
                    <View style={styles.inputContainer}>
                        <Text style={{fontWeight: 'bold', alignSelf:'center'}}>Reset Password</Text>
                        <Break />
                        <InputBlock value={this.state.email} placeholder='Enter registered email' iconName='mail' onChangeText={this.handleChangeText('email')} autoCapitalize='none' keyboardType='email-address' />
                    </View>
                    {/* ------------------FormInput----------------------------- */}
                    <Break /><Break />
                        <ButtonRounded width={width/1.1} textColor='white' onPress={() => ResetPassword(this.state.email, this.props.navigation.goBack())} name='Send Reset Password Link' loading={isLoading}/>
                    <Break />
                </View>
                {/* --------------Footer Text---------------------- */}
                <View style={{marginHorizontal:35}}>
                    <TouchableOpacity style={{flexDirection:'row', justifyContent: 'center', alignItems:'center'}} onPress={() => this.props.navigation.goBack()}>
                        <AntDesign name='arrowleft' color='blue' />
                        <Text style={{color:'blue', marginHorizontal:5}}>Back to signin</Text>
                    </TouchableOpacity>
                </View>
                {/* --------------Footer Text---------------------- */}
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        loadingState: state.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        ResetPassword : (email, callback) => dispatch(resetPassword(email, callback))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordScreen)

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        justifyContent:'center'
    },
    formContainer:{
        alignItems:'center',
        margin:25
    },
    logoSection:{
        marginBottom:10
    },
    inputContainer:{
        marginTop:25
    },
    inputSection:{
        flexDirection:'row',
        alignItems:'center',
        width:width/1.2,
        borderWidth:2,
        borderColor:'grey',
        padding:0,
        marginBottom:20
    },
})

import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Dimensions, TouchableOpacity, Alert, KeyboardAvoidingView, ScrollView, Picker } from 'react-native'
import { regEmail } from '../../services/Utility/Helpers';
import Break from '../../Components/Breaks/Break';
import ButtonRounded from '../../Components/Buttons/ButtonRounded';
import InputBlock from '../../Components/Inputs/InputBlock';
import {connect} from 'react-redux'
import { handleChangeField, SignupAction } from '../../services/redux/Actions/SignupAction';

const { width, height } = Dimensions.get('screen')
console.disableYellowBox = true
class SignupScreen extends Component {
    handleChangeText = inputType => value => {
        this.props.handleChangeField(inputType, value)
    }
    render() {
        const {authState:{program,name,username,password,confirm_password}, loadingState:{isLoading}, device:{device_token}, SignupAction} = this.props

        const handleSignup = () => {
            if(name == '' || username == '' || password == '' || confirm_password == ''){
                Alert.alert('Ooopss', 'All field must fill!')
            }else{
                if(regEmail.test(username) == false){
                    Alert.alert('Oops', 'Email is Not Correct')
                    return false
                }else{
                    if(password != confirm_password){
                        Alert.alert('Oops', 'Password & password confirmation are not the same')
                    }else{
                        SignupAction(device_token, program, name, username, password)
                        this.props.navigation.navigate('Welcome')
                    }
                }
            } 
        }

        return (
            <KeyboardAvoidingView style={styles.container}>
                {/* <Text>{JSON.stringify(this.props.loadingState)}</Text> */}
                <ScrollView>
                    <View style={styles.formContainer}>
                        <View style={styles.logoSection}>
                            <Image source={require('../../../assets/images/salt-logo.png')} style={{width:85, height:85}} />
                        </View>
                        <Text style={{ fontWeight: 'bold' }}>Assignment 4</Text>

                        {/* ------------------FormInput----------------------------- */}
                        <View style={styles.inputContainer}>
                            <Text style={{ fontWeight: 'bold', alignSelf: 'center' }}>Please register with a valid data</Text>
                            <Break />
                            <View style={styles.pickerSection}>
                                <Picker selectedValue={program} onValueChange={this.handleChangeText('program')}>
                                    <Picker.Item label="Choose Program" value='' />
                                    <Picker.Item label="Mobile Hybrid Development" value="Mobile Hybrid Development" />
                                    <Picker.Item label="Fullstack Javascript" value="Fullstack Javascript" />
                                    <Picker.Item label="Creative UI/UX" value="Creative UI/UX" />
                                </Picker>
                            </View>
                            <Break />
                            <InputBlock placeholder='Full Name' iconName='user' onChangeText={this.handleChangeText('name')} />
                            <Break />
                            <InputBlock placeholder='Email' iconName='mail' onChangeText={this.handleChangeText('username')} autoCapitalize='none' keyboardType='email-address' />
                            <Break />
                            <InputBlock placeholder='Password' onChangeText={this.handleChangeText('password')} password />
                            <Break />
                            <InputBlock placeholder='Confirmation Password' onChangeText={this.handleChangeText('confirm_password')} password />
                            <Break />
                        </View>
                        {/* ------------------FormInput----------------------------- */}
                        <Break />
                        <ButtonRounded textColor='white' onPress={handleSignup} width={width/1.1} name='Signup' loading={isLoading} />
                    </View>
                    {/* --------------Footer Text---------------------- */}
                    <View style={{ marginHorizontal: 35 }}>
                        <View style={{ flexDirection: 'row', marginVertical:0 }}>
                            <Text>Don't have an account ?</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Text style={{ fontWeight:'bold', color: 'blue', marginHorizontal: 5 }}>Sign in</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Break />
                    {/* --------------Footer Text---------------------- */}
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        loadingState: state.loading,
        authState: state.auth,
        device: state.device
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        handleChangeField: (inputType, value) => dispatch(handleChangeField(inputType, value)),
        SignupAction: (device_token, program, name, username, password) => dispatch(SignupAction(device_token, program, name, username, password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent:'center'
    },
    formContainer: {
        alignItems: 'center',
        marginTop:50,
        marginBottom:10
    },
    logoSection: {
        marginBottom: 0
    },
    inputContainer: {
        marginTop: 25
    },
    pickerSection:{
        justifyContent: 'center',
        marginHorizontal:15,
        width:width/1.1,
        borderWidth:2,
        height:45
    }
})

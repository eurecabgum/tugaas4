import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View, Alert } from 'react-native'
import { Button, Container, Content, Fab, Icon, Text} from 'native-base';
import HeaderComponent from '../../Components/Headers/HeaderComponent'
import UserItemComponent from './Components/UserItemComponent';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { concat } from 'react-native-reanimated';

class ChatScreen extends Component {
    constructor(props) {
        super(props)
        this.myId = auth().currentUser.uid
        this.state = {
            userData: [
                { id: 1, imageUrl: 'https://cdn.pixabay.com/photo/2018/08/28/13/29/avatar-3637561_1280.png', name: 'Gilang Ghifari hakiki', lastMessage: 'Doing what you', newMessage: false, isOnline: true },
                { id: 2, imageUrl: 'https://www.kindpng.com/picc/m/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png', name: 'Dika', lastMessage: 'Hello Sir', newMessage: true, isOnline: false },
                { id: 3, imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0d4CrFaUnD7NjPTdmbULFdsZ9DCcifMoj4Q&usqp=CAU', name: 'Eca', lastMessage: 'First message testing', newMessage: false, isOnline: true }
            ],
            dataUser: []
        }
        // this.userId = this.props.route.params.id
        this.getDataHistory()
    }
    
    componentDidMount() {
        
    }

    getDataHistory = async () => {
        // try {
        //     const getDataHistory = await firestore().collection('messages_history').doc(this.myId)

        //     getDataHistory.onSnapshot(documentSnapshot => {
        //         const getUid = documentSnapshot.data().history
        //         let test = []
        //         getUid.forEach(async (item, i) => {
        //             const getUser = await firestore().collection('users').doc(item.uid).get()
        //             const data = {
        //                 uid: getUser.id,
        //                 image_url: getUser.data().image_url,
        //                 name: getUser.data().name,
        //                 lastMessage: item.last_message,
        //                 newMessage: item.reading,
        //                 isOnline: true
        //             }
        //             test.push(data)
        //             this.setState({
        //                 ...this.state,
        //                 dataUser: test
        //             })
        //         })
        //     })
        // } catch (error) {
        //     console.log(error)
        // }

        const getDataHistory = await firestore().collection('messages_history').doc(this.myId).collection('history')
        getDataHistory.onSnapshot(querySnapshot => {
            let data = []
            querySnapshot.forEach(async(documentSnapshot) => {
                const getUser = await firestore().collection('users').doc(documentSnapshot.id).get()
                let initObj = {
                    uid: documentSnapshot.id,
                    image_url: getUser.data().image_url,
                    name: getUser.data().name,
                    device_token: getUser.data().device_token,
                    lastMessage: documentSnapshot.data().last_message,
                    newMessage: documentSnapshot.data().newMessage,
                    createdAt: documentSnapshot.data().createdAt
                }
                data.push(initObj)
               
                // console.log('User ID: ', documentSnapshot.id, documentSnapshot.data());
                this.setState({
                    ...this.state,
                    dataUser: data
                })
            });
        })
    }

    onLongPress = (uid) => {
        Alert.alert(
            "Further action",
            "Select remove to remove from the contact list, select destroy to delete the entire message history from both sides of the user",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Remove Item", onPress: () => this.onRemoveItem(uid)},
              { text: "Destroy", onPress: () => this.chooseDestroy(uid)},
            ],
            { cancelable: false }
        );
    }

    chooseDestroy = (uid) => {
        Alert.alert(
            "Destroy Conversation",
            "Destroy conversation is to delete all conversations between you and your chat opponent. Are you sure you want to take this action?",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "YES DESTROY", onPress: () => console.log(uid)},
            ],
            { cancelable: false }
        );
    }

    onRemoveItem = (uid) => {
        alert(uid)
    }

    render() {
        const { userData } = this.state
        const { navigation } = this.props
        return (
            <Container>
                <HeaderComponent title='Chatting' rightIcon='ellipsis-vertical-sharp' noLeft />
                {/* <Button success ><Text>Test123</Text></Button> */}
                <Content padder>
                    {this.state.dataUser.length == 0 && (
                        <Button bordered style={{alignSelf: 'center', marginTop:25}} onPress={() => this.props.navigation.navigate('NewChat')}>
                            <Text>Let's start a conversation</Text>
                        </Button>
                    )}
                    {this.state.dataUser.map((item, i) => (
                        <UserItemComponent key={i} imageUrl={item.image_url} name={item.name} lastMessage={item.lastMessage} onLongPress={()=>this.onLongPress(item.uid)}
                            newMessage={item.newMessage} createdAt={item.createdAt} onPress={() => navigation.navigate('ChatDetail', {
                                id: item.uid,
                                device_token: item.device_token,
                                photoUrl: item.image_url,
                                title: item.name,
                                isOnline: item.isOnline
                            })}
                        />
                    ))}
                </Content>
                <Fab active direction="up" containerStyle={{}} position="bottomRight" onPress={() => this.props.navigation.navigate('NewChat')}>
                    <Icon name='md-chatbubbles-sharp' />
                </Fab>
            </Container>
        )
    }
}

export default ChatScreen

const styles = StyleSheet.create({})

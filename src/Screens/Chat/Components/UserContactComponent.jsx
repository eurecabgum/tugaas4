//import liraries
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';

// create a component
const UserContactComponent = ({name, imageUrl, userProgram, onPress}) => {
    let image = imageUrl !== ''?imageUrl:'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png'
    return (
        <View>
            <List>
                <ListItem avatar onPress={onPress}>
                    <Left>
                        <Thumbnail source={{ uri: image }} />
                    </Left>
                    <Body>
                        <Text>{name}</Text>
                        <Text note>{userProgram}</Text>
                    </Body>
                </ListItem>
            </List>
        </View>
    );
};

UserContactComponent.defaultProps = {
    imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png',
    name: 'User Name',
    userProgram: 'User Program....',
    onPress: () => alert('On Press Data')
}

//make this component available to the app
export default UserContactComponent;

// define your styles
const styles = StyleSheet.create({

});


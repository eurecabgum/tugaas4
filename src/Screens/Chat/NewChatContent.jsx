import React, { Component, Fragment } from 'react'
import { StyleSheet, View } from 'react-native'
import {connect} from 'react-redux'
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';
import HeaderComponent from '../../Components/Headers/HeaderComponent'
import UserContactComponent from './Components/UserContactComponent';
import { GetDataContact } from '../../services/redux/Actions/ContactAction';

class NewChatContent extends Component {
    constructor(props){
        super(props)
        this.state = {
            isSearch: false,
            keywordSearch: '',
            userData : this.props.contact.users
        }
    }
    render() {
        const {isSearch, keywordSearch, userData} = this.state
        const {users} = this.props.contact
        const {isLoading} = this.props.loading
        const {navigation} = this.props

        const searchData = text => {
            const newData = users.filter(item => {
                const itemData = item.name.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            // alert(JSON.stringify(newData))
            this.setState({
                ...this.state,
                userData:newData,
                keywordSearch:text
            })
        };
     
        return (
            <Fragment>
                {
                    isSearch?
                        <Header searchBar rounded iosBarStyle='light-content'>
                            <Item>
                                <Icon name="ios-search" />
                                <Input placeholder="Search Contact" value={keywordSearch} autoFocus={true} onChangeText={(text) => searchData(text)}  />
                                <Icon name="ios-close" onPress={() => this.setState({isSearch:false})} />
                            </Item>
                        </Header>
                        :
                        <HeaderComponent title='Choose Contact' subtitle={`${users.length} contact`} leftIcon='arrow-back' leftAction={() => navigation.goBack()} rightIcon='ios-search' rightAction={() => this.setState({isSearch: true})} />
                }
                {/* <Text>{JSON.stringify(users)}</Text> */}
                {isLoading && (
                    <Text style={{margin:10, color:'grey'}}>Loading Data...</Text>
                )}
                {!isLoading && (
                    <Fragment>
                        {userData.map((item, i) => (
                            <UserContactComponent key={i} imageUrl={item.image_url} name={item.name} userProgram={item.program}
                                onPress={() => navigation.navigate('ChatDetail',{
                                    id: item.id,
                                    photoUrl: item.image_url,
                                    title: item.name,
                                    isOnline: item.online
                                })}/>
                        ))}
                    </Fragment>
                )}
                {/* <Text>test</Text> */}
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        contact: state.contact,
        loading: state.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        GetDataContact: () => dispatch(GetDataContact()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewChatContent)

const styles = StyleSheet.create({})

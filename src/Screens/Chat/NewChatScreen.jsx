import React, { Component, Fragment } from 'react'
import { Text, StyleSheet, View, Content } from 'react-native'
import { Container, Spinner } from 'native-base'
import HeaderComponent from '../../Components/Headers/HeaderComponent';
import {connect} from 'react-redux'
import NewChatContent from './NewChatContent';
import LoadingSpinner from '../../Components/loadings/LoadingSpinner';
import { GetDataContact } from '../../services/redux/Actions/ContactAction';
import auth from '@react-native-firebase/auth';

class NewChatScreen extends Component {
    state = {
        content: false
    }
    componentDidMount() {
        this.props.GetDataContact(auth().currentUser.email)
        setTimeout(() => {
            this.setState({ content: true })
        }, 300);
    }
    render() {
        const {content} = this.state
        return (
            <Container>
                {content?(<NewChatContent navigation={this.props.navigation} />):(<LoadingSpinner />)}
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        loading: state.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        GetDataContact: (email) => dispatch(GetDataContact(email))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewChatScreen)

const styles = StyleSheet.create({})

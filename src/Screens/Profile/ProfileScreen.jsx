import React, { Component } from 'react'
import { Image, StyleSheet, View } from 'react-native'
import { Container, Content, Button, Text } from 'native-base'
import HeaderComponent from '../../Components/Headers/HeaderComponent'
// import ImagePicker from 'react-native-image-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import axios from 'axios'
import { Fragment } from 'react';

class ProfileScreen extends Component {
    state = {
        sourceImage: '',
        loading:false,
        urlShow:''
    }
    componentWillUnmount(){
        console.log('komponen di lepas!')
    }
    sendNotif = async (type) => {
        if(type === 'toAndroidSimulator'){
            var token = 'dWYYPtD0RQuIHE5uZc3X0F:APA91bHb0vvKkdqy9vxgyYIG1Tr0TZBhrndxIc-w-y7MT_h_QIDQUQMg5qd12ZPBzoNmXrPUejgABVqQ5NlcyiDOmjLgZnn7eqxPKemNT09hlXUe-HFYi11-WA__DImm4cfcFdqR6FQV'
            var title = 'Memu To Android Emulator'
            var body = 'Testing notification from memu to android emulator'
        }else{
            var token = 'fEYTDPyzSoOMNwE1EjnNCe:APA91bEmnI6SmB-ciBRltkVQHHVXCfDugg0mrioq08A3hmclNeytBt7hpAeATbuZVeMUEIJuM23Vd3WnP62xU-wq8RSEu3AmrdWiMzKtXScKPJr9klgpAx0HijUKtnwhqEmd7wT17U8N'
            var title = 'Android Emulator To Memuplay'
            var body = 'Testing notification from android simulator to memuplay'
        }
        try {
            let data = {
                to: token,
                notification:{
                    title: title,
                    body: body,
                    mutable_content: true,
                    sound: 'Tri-tone',
                    image: 'https://img.icons8.com/cotton/2x/appointment-reminders.png'
                },
                data:{
                    url: 'https://img.icons8.com/cotton/2x/appointment-reminders.png',
                }
             }
            const response = await axios.post('https://fcm.googleapis.com/fcm/send', data, {
                headers:{
                    'Content-Type': 'application/json',
                    'Authorization': 'key=AAAApxEJbkw:APA91bFkLwX8bxdxUZ2p0htQ5EpgenmqOqaJ-Phcm_qGQKhQtDymYXIu723Bt7QEZBU_KUJ-Jtdb5xGrug-Sxna71dxprOtDemCIGozhbk8gaMQ91RGK0OdVwgXr5IRvbFKx2fZQlCGX'
                }
            });
            console.log(response);
            alert(JSON.stringify(response))
        }catch(error){
            console.error(error);
        }
    }

    
    upload = () => {
        const options = {
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
        launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
              const source = { uri: response.uri };
          
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          
              this.setState({
                sourceImage: source.uri,
              });
            }
        })
    }

    sendToCloud = async () => {
        this.setState({loading:true})
        try{
            const reference = storage().ref(`Upload/${Date.now()}`);
            const pathToFile = this.state.sourceImage;
            const uploading = await reference.putFile(pathToFile);
            if(uploading){
                this.setState({loading:false})
                alert('Gambar berhasil di upload')
            }
        }catch(error){
            this.setState({loading:false})
            alert(error)
        }
    }

    showImage = async () => {
        try{
            const url = await storage().ref('Upload/1614186912053').getDownloadURL();
            if(url){
                this.setState({urlShow:url})
            }
        }catch(error){
            alert(error)
        }

    }

    render() {
        return (
            <Container>
                <HeaderComponent title='Profile' leftIcon='ios-person'/>
                <Content padder>
                    <Text>Profile Screen</Text>
                    <Button bordered style={{alignSelf: 'center', marginTop:25}} onPress={() => this.sendNotif('toAndroidSimulator')}>
                        <Text>Send Notif to Android Emulator</Text>
                    </Button>
                    <Button warning bordered style={{alignSelf: 'center', marginTop:25}} onPress={() => this.sendNotif('toMemuPlay')}>
                        <Text>Send Notif to Memuplay</Text>
                    </Button>
                    <Button bordered style={{alignSelf: 'center', marginTop:25}} onPress={() => this.props.navigation.navigate('Carousel')}>
                        <Text>Carousel</Text>
                    </Button>
                    <Button danger bordered style={{alignSelf: 'center', marginTop:25}} onPress={this.upload}>
                        <Text>Upload Image</Text>
                    </Button>
                    <Button success bordered style={{alignSelf: 'center', marginTop:25}} onPress={this.showImage}>
                        <Text>Show Sample Image</Text>
                    </Button>
                    {this.state.sourceImage != '' && (
                        <Fragment>
                            <Image source={{uri: this.state.sourceImage}} style={{height:100}} />
                            <Button onPress={this.sendToCloud} disabled={this.state.loading}>
                                <Text>{this.state.loading?'MENGUPLOAD':'UPLOAD'}</Text>
                            </Button>
                        </Fragment>
                    )}
                    {this.state.urlShow !='' && (
                        <Image source={{uri: this.state.urlShow}} style={{height:250}} />                  
                    )}
                    <Text>{JSON.stringify(this.state)}</Text>
                </Content>
            </Container>
        )
    }
}

export default ProfileScreen

const styles = StyleSheet.create({})

import firestore from '@react-native-firebase/firestore';

const UpdateChat = (data) => dispatch => {
    dispatch({type: 'ADD-ITEM-CHAT', payload: data})
}

const GetChat = (data) => dispatch => {
    dispatch({type: 'GET-ITEM-CHAT', payload:data})
}

export {UpdateChat, GetChat}
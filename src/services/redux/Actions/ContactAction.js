import firestore from '@react-native-firebase/firestore';

const GetDataContact = (email) => async dispatch => {
    dispatch({type:'LOADING'})
    const query = await firestore().collection('users').where('email', '!=', email)
    query.onSnapshot(querySnapshot => {
        let contact = [];
        querySnapshot.forEach(doc => {
            contact.push({
                id: doc.id,
                name: doc.data().name,
                image_url: doc.data().image_url,
                device_token: doc.data().device_token,
                program: doc.data().program,
                online: doc.data().online
            });
        });
        dispatch({type: 'NO-LOADING'})
        dispatch({ type: 'GET-DATA-CONTACT', payload: contact })
    });
}

export {GetDataContact}
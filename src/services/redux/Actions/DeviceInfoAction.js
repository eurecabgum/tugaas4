const UpdateDeviceAction = (data) => dispatch => {
    dispatch({type: 'UPDATE-DEVICE-INFO', payload: data})
}

export {UpdateDeviceAction}
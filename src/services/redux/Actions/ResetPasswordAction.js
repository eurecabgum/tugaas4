import auth from '@react-native-firebase/auth';
import { Alert, ToastAndroid } from 'react-native';
import { regEmail } from '../../Utility/Helpers';

const resetPassword = (email, callback) => dispatch => {
    if(email == ''){
        ToastAndroid.showWithGravity('Please enter your email first',ToastAndroid.SHORT,ToastAndroid.TOP);
    }else{
        if(regEmail.test(email) == false){
            ToastAndroid.showWithGravity('Email not valid',ToastAndroid.SHORT,ToastAndroid.TOP);
            return false
        }else{
            dispatch({type: 'LOADING'})
            auth().sendPasswordResetEmail(email).then(() => {
                Alert.alert('Success', 'Check your email, to get a password reset link')
                dispatch({type:'NO-LOADING'})
                if(callback) return callbak
            }).catch(function(error) {
                dispatch({type: 'NO-LOADING'})
                switch(error.code){
                    case 'auth/email-already-in-use':
                        return Alert.alert('Oops', 'That email address is already in use!')
                    case 'auth/invalid-email':
                        return Alert.alert('Oops', 'That email address is invalid!')
                    case 'auth/user-not-found':
                        return Alert.alert('Oops', 'Ther is no user record corresponding to this identifier.')
                    default:
                        return Alert.alert('Oops', error.toString())
                }
            });
        }
    } 
}

export {resetPassword}
import auth from '@react-native-firebase/auth';
import { Alert, Keyboard } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { regEmail } from '../../Utility/Helpers';

const SigninAction = (device_token, email, password) => async dispatch => {
    Keyboard.dismiss()
    if (email === '' || password === '') {
        Alert.alert('Sorry...', 'Please fill username & password')

    } else {
        if (regEmail.test(email) === false) {
            Alert.alert('Oops', 'Email is Not Correct')
            return false
        } else {
            dispatch({ type: 'LOADING' })
            try {
                let login = await auth().signInWithEmailAndPassword(email, password)
                console.log(login.user.uid)
                firestore().collection('users').doc(login.user.uid)
                .update({
                    device_token: device_token,
                    online: true
                })
                dispatch({ type: 'NO-LOADING' })
            } catch (error) {
                dispatch({ type: 'NO-LOADING' })
                switch (error.code) {
                    case 'auth/user-not-found':
                        return Alert.alert('Oops', 'There is no user record corresponding to this identifier')
                    default:
                        return Alert.alert('Oops', 'Incorrect email or password')
                }
            }
        }
    }
}

export { SigninAction }
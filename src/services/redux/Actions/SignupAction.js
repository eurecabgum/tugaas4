import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { Alert } from 'react-native';

const handleChangeField = (inputType, value) => dispatch => {
    dispatch({type: 'CHANGE-FIELD', payload:{inputType, value}})
}

const SignupAction = (device_token, program, name, username, password) => async dispatch => {
    dispatch({type:'LOADING'})
    try {
        const register = await auth().createUserWithEmailAndPassword(username, password)
        console.log(register.user.uid)
        Alert.alert('Thanks', 'Your account has been successfully registered')
        register.user.updateProfile({
            displayName: name
        })
        firestore().collection('users').doc(register.user.uid).set({
            program: program,
            email: username,
            name: name,
            image_url: '',
            address: '',
            gender: '',
            birthday: '',
            device_token: device_token,
            online: false
        })
        dispatch({type: 'NO-LOADING'})
    }catch(error){
        dispatch({type: 'NO-LOADING'})
        switch(error.code){
            case 'auth/email-already-in-use':
                return Alert.alert('Oops', 'That email address is already in use!')
            case 'auth/invalid-email':
                return Alert.alert('Oops', 'That email address is invalid!')
            case 'auth/weak-password':
                return Alert.alert('Oops', 'Password should be at least 6 characters!')
            default:
                return Alert.alert('Oops', error.toString())
        }   
    }
}

export {handleChangeField, SignupAction}
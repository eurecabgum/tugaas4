const initialState = {
    // users:[
    //     {
    //         id: 'eE8MtGb1asP8E4ie46VSHiEVjuD3', name: 'Iqbal', messages:[
    //             {
    //                 _id: 1,
    //                 text: 'Hello Salt. . .',
    //                 createdAt: new Date(1610473075*1000),
    //                 user: {
    //                     _id: 2,
    //                     name: 'React Native',
    //                     avatar: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png',
    //                 },
    //             },
    //         ]
    //     },
    //     {
    //         id: 'avBIasdmvcOe3ftyhfM3RbdK0hV2', name: 'Gilang', messages:[
    //             {
    //                 _id: 1,
    //                 text: 'Hello Salt. . . 2',
    //                 createdAt: new Date(1610473075*1000),
    //                 user: {
    //                     _id: 2,
    //                     name: 'React Native',
    //                     avatar: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png',
    //                 },
    //             },
    //         ]
    //     }
    // ]
    messages: [
        {
            _id: 1,
            text: 'Hello Salt. . .',
            createdAt: new Date(1610473075*1000),
            user: {
                _id: 2,
                name: 'React Native',
                avatar: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png',
            },
        },
    ],
}

const ChatReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD-ITEM-CHAT':
            return{
                messages: action.payload
            }
        case 'GET-ITEM-CHAT':
            return{
                messages:[
                    {
                        _id: 1,
                        text: 'Hello Salt. . .',
                        createdAt: new Date(1610473075*1000),
                        user: {
                            _id: 2,
                            name: 'React Native',
                            avatar: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png',
                        },
                    },
             
                ]
            }
        default:
            return state
    }
}

export default ChatReducer
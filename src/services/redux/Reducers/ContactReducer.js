const initialState = {
    users: []
}

const ContactReducer = (state = { users: [] }, action) => {
    switch (action.type) {
        case 'GET-DATA-CONTACT':
            return {
                ...state,
                users: action.payload
            }
        case 'SEARCH-CONTACT':
            return {
                ...state,
                users: action.payload
            }
        default:
            return state
    }
}

export default ContactReducer

const initialState = {
    device_token: '',
    device_id: '',
    manufacture: '',
    battery_level: '',
    device_name: '',
    brand: '',
    mac_address: '',
}

const DeviceInfoReducer = (state= initialState, action) => {
    switch (action.type) {
        case 'UPDATE-DEVICE-INFO':
            return{
                ...state,
                device_token: action.payload.device_token,
                device_id: action.payload.device_id,
                manufacture: action.payload.manufacture,
                battery_level: action.payload.battery_level,
                device_name: action.payload.device_name,
                brand: action.payload.brand,
                mac_address: action.payload.mac_address,
            }
        default:
            return state;
    }
} 

export default DeviceInfoReducer
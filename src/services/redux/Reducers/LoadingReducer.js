const initialState = {
    isLoading: false
}

const LoadingReducer = (state=initialState, action) => {
    switch(action.type){
        case 'LOADING':
            return{
                isLoading: true
            }
        case 'NO-LOADING':
            return{
                isLoading: false
            }
        default:
            return state
    }
}

export default LoadingReducer
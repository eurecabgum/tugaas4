import {combineReducers} from 'redux'
import LoadingReducer from './LoadingReducer'
import AuthReducer from './AuthReducer'
import DeviceInfoReducer from './DeviceInfoReducer'
import ContactReducer from './ContactReducer'
import ChatReducer from './ChatReducer'

const reducer = combineReducers({
    loading: LoadingReducer,
    auth: AuthReducer,
    device: DeviceInfoReducer,
    contact: ContactReducer,
    chat: ChatReducer
})

export default reducer